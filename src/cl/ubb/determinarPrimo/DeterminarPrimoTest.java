package cl.ubb.determinarPrimo;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

public class DeterminarPrimoTest {

	@Test
	public void IngesarCeroRetornarPrimoFalso() {
		NumeroPrimo primo= new NumeroPrimo();
		
		boolean resultado;
		
		resultado = primo.DeterminarPrimo(0);
		
		assertThat(resultado,is(false));
	}

}
